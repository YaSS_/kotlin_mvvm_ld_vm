package com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.data

class QuoteDataBase private constructor() {

    var quoteDao = QuoteDao()
        private set

    companion object {
        @Volatile
        private var instance: QuoteDataBase? = null

        fun getInstance() =
            instance ?: synchronized(lock = this) {
                instance ?: QuoteDataBase().also { instance = it }
            }
    }
}