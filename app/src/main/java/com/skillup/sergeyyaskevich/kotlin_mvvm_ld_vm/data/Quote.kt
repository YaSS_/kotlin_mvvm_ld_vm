package com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.data

data class Quote(val quote: String,
                 val author: String) {
    override fun toString(): String {
        return "$quote - $author"
    }
}