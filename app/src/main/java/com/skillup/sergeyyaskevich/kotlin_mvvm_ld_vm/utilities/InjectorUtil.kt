package com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.utilities

import com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.data.QuoteDao
import com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.data.QuoteDataBase
import com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.data.QuoteRepository
import com.skillup.sergeyyaskevich.kotlin_mvvm_ld_vm.ui.quotes.QuotesViewModelFactory

object InjectorUtil {

    fun provideQuotesViewModelFactory() : QuotesViewModelFactory{
        val quotesRepository = QuoteRepository.getInstance(QuoteDataBase.getInstance().quoteDao)
        return QuotesViewModelFactory(quotesRepository)
    }
}